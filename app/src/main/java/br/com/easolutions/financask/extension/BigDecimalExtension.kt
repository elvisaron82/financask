package br.com.easolutions.financask.extension

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*

/**
 * Created by Elvis Aron Andrade on 30/12/18.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */

fun BigDecimal.formataParaBrasileiro() : String? {
    return DecimalFormat.getCurrencyInstance(Locale("pt", "br")).format(this)
        .replace("R$", "R$ ")
        .replace("-R$ ", "R$ -")
}
package br.com.easolutions.financask.ui.dialog

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import br.com.easolutions.financask.R
import br.com.easolutions.financask.extension.converteParaCalendar
import br.com.easolutions.financask.extension.formataParaBrasileiro
import br.com.easolutions.financask.model.Tipo
import br.com.easolutions.financask.model.Transacao
import kotlinx.android.synthetic.main.form_transacao.view.*
import java.math.BigDecimal
import java.util.*

/**
 * Created by Elvis Aron Andrade on 12/01/19.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */
abstract class FormularioTransacaoDialog(private val context: Context, private val viewGroup: ViewGroup?) {
    private val viewCriada = criaLayout()
    protected val campoValor = viewCriada.form_transacao_valor
    protected val campoData = viewCriada.form_transacao_data
    protected val campoCategoria = viewCriada.form_transacao_categoria
    protected abstract val tituloBotaoPositive: String

    fun chama(tipo: Tipo, delegate: (transacao: Transacao) -> Unit) {
        configuraCampoData()
        configuraCampoCategoria(tipo)
        configuraFormulario(tipo, delegate)
    }

    fun configuraFormulario(tipo: Tipo, delegate: (transacao: Transacao) -> Unit) {
        val titulo = tituloPor(tipo)

        AlertDialog.Builder(context)
            .setTitle(titulo)
            .setView(viewCriada)
            .setPositiveButton(tituloBotaoPositive) { _, _ ->
                val valorEmTexto = campoValor.text.toString()
                val dataEmTexto = campoData.text.toString()
                val categoriaEmTexto = campoCategoria.selectedItem.toString()
                val valor = converteCampoValor(valorEmTexto)
                val data = dataEmTexto.converteParaCalendar()

                val transacao = Transacao(valor, categoriaEmTexto, tipo, data)

                delegate(transacao)
            }
            .setNegativeButton("Cancelar", null)
            .show()
    }

    abstract protected fun tituloPor(tipo: Tipo): Int
//    {
//        return if (tipo == Tipo.RECEITA) {
//            R.string.adiciona_receita
//        } else {
//            R.string.adiciona_despesa
//        }
//    }

    private fun converteCampoValor(valorEmTexto: String): BigDecimal {
        return try {
            BigDecimal(valorEmTexto)
        } catch (exceptrion: Exception) {
            Toast.makeText(
                context,
                "Falha na conversão de valor",
                Toast.LENGTH_LONG
            )
                .show()
            BigDecimal.ZERO
        }
    }

    private fun configuraCampoCategoria(tipo: Tipo) {

        val categorias = categoriasPor(tipo)

        val adapter = ArrayAdapter.createFromResource(
            context, categorias,
            android.R.layout.simple_spinner_dropdown_item
        )

        campoCategoria.adapter = adapter
    }

    protected fun categoriasPor(tipo: Tipo): Int {
        return if (tipo == Tipo.RECEITA) {
            R.array.categorias_de_receita
        } else {
            R.array.categorias_de_despesa
        }
    }

    private fun configuraCampoData() {
        val dataAtual = Calendar.getInstance()

        val ano = dataAtual.get(Calendar.YEAR)
        val mes = dataAtual.get(Calendar.MONTH)
        val dia = dataAtual.get(Calendar.DAY_OF_MONTH)

        campoData.setText(dataAtual.formataParaBrasileiro())

        campoData.setOnClickListener {

            DatePickerDialog(context, { _, ano, mes, dia ->

                val dataSelecionada = Calendar.getInstance()
                // modifica data do dia inserindo a data que o usuário selecionou
                dataSelecionada.set(ano, mes, dia)
                // seta no layout a data que o usuário selecionou e formata para brasileiro
                campoData.setText(dataSelecionada.formataParaBrasileiro())
            }, ano, mes, dia).show()
        }
    }

    private fun criaLayout(): View {
        return LayoutInflater.from(context)
            .inflate(R.layout.form_transacao, viewGroup, false)

    }
}
package br.com.easolutions.financask.model

import java.math.BigDecimal
import java.util.*

/**
 * Created by Elvis Aron Andrade on 16/12/18.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */
class Transacao(
    val valor: BigDecimal,
    val categoria: String = "Indefinida",
    val tipo: Tipo,
    val data: Calendar = Calendar.getInstance()
)
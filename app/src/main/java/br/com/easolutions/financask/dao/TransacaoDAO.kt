package br.com.easolutions.financask.dao

import br.com.easolutions.financask.model.Transacao

/**
 * Created by Elvis Aron Andrade on 12/01/19.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */
class TransacaoDAO {

    val transacoes = Companion.transacoes

    companion object {
        private val transacoes: MutableList<Transacao> = mutableListOf()
    }

    fun adiciona(transacao: Transacao) {
        Companion.transacoes.add(transacao)
    }

    fun altera(transacao: Transacao, posicao: Int) {
        Companion.transacoes[posicao] = transacao
    }

    fun remove(posicao: Int) {
        Companion.transacoes.removeAt(posicao)
    }
}
package br.com.easolutions.financask.extension

import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Elvis Aron Andrade on 31/12/18.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */

fun String.limitaEmAte(caracteres: Int): String {
    if (this.length > caracteres) {
        return "${this.substring(0, caracteres)}..."
    }
    return this
}

fun String.converteParaCalendar() : Calendar {
    val formatoBrasileiro = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    val dataConvertida = formatoBrasileiro.parse(this)
    val data = Calendar.getInstance()
    data.time = dataConvertida
    return data
}

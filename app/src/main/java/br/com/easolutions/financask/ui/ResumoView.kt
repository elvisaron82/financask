package br.com.easolutions.financask.ui

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import br.com.easolutions.financask.R
import br.com.easolutions.financask.extension.formataParaBrasileiro
import br.com.easolutions.financask.model.Resumo
import br.com.easolutions.financask.model.Transacao
import kotlinx.android.synthetic.main.resumo_card.view.*
import java.math.BigDecimal

/**
 * Created by Elvis Aron Andrade on 01/01/19.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */
class ResumoView(context : Context,
                 private val view: View,
                 listaDeTransacoes: List<Transacao>) {

    private val resumo: Resumo = Resumo(listaDeTransacoes)
    private val colorReceita = ContextCompat.getColor(context, R.color.receita)
    private val colorDespesa = ContextCompat.getColor(context, R.color.despesa)

    fun atualiza() {
        adicionarReceita()
        adicionarDespesa()
        adicionarTotal()
    }

    private fun adicionarReceita() {
        val totalReceita = resumo.receita
        with(view.resumo_card_receita) {
            setTextColor(colorReceita)
            text = totalReceita.formataParaBrasileiro()
        }
    }


    private fun adicionarDespesa() {
        val totalDespesa = resumo.despesa
        with(view.resumo_card_despesa) {
            setTextColor(colorDespesa)
            text = totalDespesa.formataParaBrasileiro()
        }
    }

    private fun adicionarTotal() {
        val total = resumo.total
        val cor = corPorValor(total)
        with(view.resumo_card_total) {
            setTextColor(cor)
            text = total.formataParaBrasileiro()
        }
    }

    private fun corPorValor(total: BigDecimal): Int {
        return if (total >= BigDecimal.ZERO) {
            colorReceita
        } else {
            colorDespesa
        }
    }
}
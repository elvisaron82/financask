package br.com.easolutions.financask.ui.dialog

import android.content.Context
import android.view.ViewGroup
import br.com.easolutions.financask.R
import br.com.easolutions.financask.extension.formataParaBrasileiro
import br.com.easolutions.financask.model.Tipo
import br.com.easolutions.financask.model.Transacao

/**Copyright 2019 Rede S.A.
 *************************************************************
 *Descrição: br.com.easolutions.financask.ui.dialog
 *Autor    : Elvis Aron Andrade
 *Data     : 09/01/19 12:04
 *Empresa  : Iteris
 **************************************************************/
class AlteraTransacaoDialog(
    viewGroup: ViewGroup,
    private val context: Context
) : FormularioTransacaoDialog(context, viewGroup) {
    override val tituloBotaoPositive: String
        get() = "Alterar"

    override fun tituloPor(tipo: Tipo): Int {
        return if (tipo == Tipo.RECEITA) {
            R.string.altera_receita
        } else {
            R.string.altera_despesa
        }
    }

    fun chama(transacao: Transacao, delegate: (transacao: Transacao) -> Unit) {
        val tipo = transacao.tipo
        super.chama(tipo, delegate)
        inicializaCampos(transacao)
    }

    private fun inicializaCampos(
        transacao: Transacao
    ) {
        val tipo = transacao.tipo
        campoValor.setText(transacao.valor.toString())
        campoData.setText(transacao.data.formataParaBrasileiro())
        inicializaCampoCategoria(tipo, transacao)
    }

    private fun inicializaCampoCategoria(
        tipo: Tipo,
        transacao: Transacao
    ) {
        val categoriasRetornadas = context.resources.getStringArray(categoriasPor(tipo))
        val posicaoCategoria = categoriasRetornadas.indexOf(transacao.categoria)
        campoCategoria.setSelection(posicaoCategoria, true)
    }
}
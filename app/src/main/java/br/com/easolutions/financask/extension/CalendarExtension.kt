package br.com.easolutions.financask.extension

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Elvis Aron Andrade on 29/12/18.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */

fun Calendar.formataParaBrasileiro(): String? {
    val formatoBrasileiro = "dd/MM/yyyy"
    val simpleDataFormat = SimpleDateFormat(formatoBrasileiro, Locale.getDefault())
    return simpleDataFormat.format(time)
}
package br.com.easolutions.financask.ui.dialog

import android.content.Context
import android.view.ViewGroup
import br.com.easolutions.financask.R
import br.com.easolutions.financask.model.Tipo

/**Copyright 2019 Rede S.A.
 *************************************************************
 *Descrição: br.com.easolutions.financask.ui.dialog
 *Autor    : Elvis Aron Andrade
 *Data     : 09/01/19 12:04
 *Empresa  : Iteris
 **************************************************************/
class AdicionaTransacaoDialog(
    viewGroup: ViewGroup,
    context: Context
) : FormularioTransacaoDialog(context, viewGroup) {

    override val tituloBotaoPositive: String
        get() = "Adicionar"

    override fun tituloPor(tipo: Tipo): Int {
        return if (tipo == Tipo.RECEITA) {
            R.string.adiciona_receita
        } else {
            R.string.adiciona_despesa
        }
    }

}
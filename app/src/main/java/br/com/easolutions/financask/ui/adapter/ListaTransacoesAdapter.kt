package br.com.easolutions.financask.ui.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import br.com.easolutions.financask.R
import br.com.easolutions.financask.extension.formataParaBrasileiro
import br.com.easolutions.financask.extension.limitaEmAte
import br.com.easolutions.financask.model.Tipo
import br.com.easolutions.financask.model.Transacao
import kotlinx.android.synthetic.main.transacao_item.view.*

private val LIMITE_DE_TAMANHO_DA_CATEGORIA = 14

class ListaTransacoesAdapter(
    private val transacoes: List<Transacao>,
    private val context: Context) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val viewCriada = LayoutInflater.from(context)
            .inflate(R.layout.transacao_item, parent, false)

        val transacao = transacoes[position]

        adicionarValor(transacao, viewCriada)
        adicionarIcone(transacao, viewCriada)
        adicionarCategoria(transacao, viewCriada)
        adicionarData(transacao, viewCriada)

        return viewCriada
    }

    private fun adicionarData(transacao: Transacao, viewCriada: View) {
        viewCriada.transacao_data.text = transacao.data.formataParaBrasileiro()
    }

    private fun adicionarCategoria(transacao: Transacao, viewCriada: View) {
        viewCriada.transacao_categoria.text = transacao.categoria.limitaEmAte(LIMITE_DE_TAMANHO_DA_CATEGORIA)
    }

    private fun adicionarIcone(transacao: Transacao, viewCriada: View) {
        val icone: Int = iconePorTipoDeTransacao(transacao.tipo)
        viewCriada.transacao_icone.setBackgroundResource(icone)
    }

    private fun adicionarValor(transacao: Transacao, viewCriada: View) {
        val cor: Int = corPorTipoDeTransacao(transacao.tipo)
        viewCriada.transacao_valor.setTextColor(cor)
        viewCriada.transacao_valor.text = transacao.valor.formataParaBrasileiro()
    }

    private fun corPorTipoDeTransacao(tipo: Tipo): Int {
        return if (tipo == Tipo.RECEITA) {
            ContextCompat.getColor(context, R.color.receita)
        } else {
            ContextCompat.getColor(context, R.color.despesa)
        }
    }

    private fun iconePorTipoDeTransacao(tipo: Tipo): Int {
        return if (tipo == Tipo.RECEITA) {
            R.drawable.icone_transacao_item_receita
        } else {
            R.drawable.icone_transacao_item_despesa
        }
    }

    override fun getItem(position: Int): Transacao {
        return transacoes[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return transacoes.size
    }

}
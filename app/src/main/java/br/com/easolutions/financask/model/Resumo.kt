package br.com.easolutions.financask.model

import java.math.BigDecimal

/**
 * Created by Elvis Aron Andrade on 01/01/19.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */
class Resumo(private val listaDeTransacoes: List<Transacao>) {

    val receita get() = somaPorTipo(Tipo.RECEITA)

    val despesa get() = somaPorTipo(Tipo.DESPESA)

    val total get() = receita.subtract(despesa)

    private fun somaPorTipo(tipo : Tipo) : BigDecimal {
        val somaDeTransacoesPorTipo = listaDeTransacoes.filter { it.tipo == tipo }.sumByDouble { it.valor.toDouble() }
        return BigDecimal(somaDeTransacoesPorTipo)
    }
}
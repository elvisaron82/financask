package br.com.easolutions.financask.model

/**
 * Created by Elvis Aron Andrade on 29/12/18.
 * Company:   EA Solutions
 * E-mail :   elvis.andrade@easolutions.com.br
 */
enum class Tipo {
    RECEITA,
    DESPESA
}